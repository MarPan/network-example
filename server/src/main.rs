use byteorder::{WriteBytesExt, BigEndian};
use crossbeam::deque::{Injector};
use std::sync::{Arc, Mutex};
use std::net::{UdpSocket, SocketAddr};
use std::convert::TryFrom;
use std::collections::{HashMap};
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::time::{Duration, Instant};
use std::ops::{Sub, Add};
use std::io::ErrorKind;

type KeysState = HashMap<u16,bool>; // TODO: This might be simplified to KeysPressed and a HashSet
type PlayerPosition = (f64, f64);
type Connections = HashMap<SocketAddr, Client>;

struct Client {
    key_state: KeysState,
    last_datagram_id: u16,
    position: PlayerPosition,
    id: u16,
    last_datagram_timestamp: Instant,
}

// TODO: every client needs an id
impl Client {
    pub fn new(id: u16) -> Client {
        Client {
            key_state: Default::default(),
            last_datagram_id: 0,
            position: (300.0, 300.0),
            id,
            last_datagram_timestamp: Instant::now(),
        }
    }
}

#[allow(unreachable_code)]
fn main() -> std::io::Result<()>
{
    let socket = UdpSocket::bind("127.0.0.1:7878").expect("Failed to bind socket");

    let clients = Arc::new(Mutex::new(Connections::new()));
    // TODO: Data sources can be kept in a map. This way  and only indexes need to be saved
    //       in the queue.
    // This is a FIFO queue. Adding and removing tasks from that can be done even if the
    // variable is not mutable.
    let scheduled_replies:Injector<([u8; 512], SocketAddr)> = Injector::new();

    crossbeam::scope(|thread_spawner| {
        // This thread continuously reads data from the UDP socket and processes them.
        thread_spawner.spawn(|_| {
            // Data from UDP datagrams is read into this buffer
            let buffer: [u8; 512] = [0; 512];
            let mut last_client_id: u16 = 0;

            loop {
                read_one_datagram(&socket, buffer, clients.clone(), &mut last_client_id);
            }
        });

        // This threads sends datagrams stored in scheduled_replies (a FIFO queue that can be
        // accessed from different threads) wit a delay to simulate a lag.
        thread_spawner.spawn(|_|  {
            let mut replies_and_times: Vec<(([u8; 512], SocketAddr), Instant)> = Vec::new();
            loop {
                send_replies(&scheduled_replies, &mut replies_and_times, &socket);
            }
        });

        // This thread is a main game loop. It will read the keyboard state of each player
        // and process their actions. As a result an entry should be added to `scheduled_replies`.
        thread_spawner.spawn(|_|  {
            // NOTE: all times are stored in nanoseconds
            let dt = 10 * 1000 * 1000; // 10 ms should pass between each logic tick.
            let mut last_iteration_time = Instant::now();
            let speed: u16 = 500;
            let mut accumulator = 0;

            loop {
                let current_time = Instant::now();
                let delta = current_time.duration_since(last_iteration_time).as_nanos();
                last_iteration_time = current_time;

                accumulator += delta;

                while accumulator >= dt {
                    // drop not responding clients
                    let now = Instant::now();
                    clients.lock().unwrap().retain(|_, client| {
                        match now.checked_duration_since(client.last_datagram_timestamp) {
                            Some(time_difference) => (time_difference.as_secs() < 1),
                            None => true
                        }
                    });

                    accumulator -= dt;
                    for (_, mut client) in clients.lock().unwrap().iter_mut() {
                        logic_tick(&mut client,
                                   dt as f64 / 1000.0,
                                   speed);
                    }

                    /* new datagram structure:
                    <last_client_datagram_id><client_id><player_count>[<player_id><posX><posY>] */

                    // Prepare common part of datagrams for clients
                    // (empty 4 bytes)<player_count>[<player_id><posX><posY>]
                    let mut response_buffer: [u8; 512] = [0; 512];
                    // player count
                    (&mut response_buffer[4..6]).write_u16::<BigEndian>(clients.lock().unwrap().len() as u16).unwrap();

                    let id_size = 2;
                    let coordinate_size = 8;
                    let entry_size = id_size + coordinate_size + coordinate_size; // playerId is u16, position is two f64
                    for (i, (_, client)) in clients.lock().unwrap().iter_mut().enumerate() {
                        let mut buffer_start = 6 + i*entry_size;
                        let mut buffer_end = buffer_start + id_size;
                        (&mut response_buffer[buffer_start..buffer_end]).
                            write_u16::<BigEndian>(client.id).unwrap();
                        buffer_start += id_size;
                        buffer_end = buffer_start + coordinate_size;
                        (&mut response_buffer[buffer_start..buffer_end]).
                            write_f64::<BigEndian>(client.position.0).unwrap();
                        buffer_start += coordinate_size;
                        buffer_end = buffer_start + coordinate_size;
                        (&mut response_buffer[buffer_start..buffer_end]).
                            write_f64::<BigEndian>(client.position.1).unwrap();
                    }
                    // common buffer ready. Now fill the 0-4 bytes with <last_client_datagram_id><client_id>
                    for (address, client) in clients.lock().unwrap().iter_mut() {
                        // array of u8 is Copy, because u8 is copy.
                        let mut client_buffer = response_buffer;
                        (&mut client_buffer[0..2]).write_u16::<BigEndian>(client.last_datagram_id).unwrap();
                        (&mut client_buffer[2..4]).write_u16::<BigEndian>(client.id).unwrap();

                        // Create a response datagram
                        scheduled_replies.push((client_buffer, *address));
                    }
                }
            }
        });
    }).expect("Thread related error.");

    Ok(())
}

fn send_replies(scheduled_replies: &Injector<([u8; 512], SocketAddr)>,
                replies_and_times: &mut Vec<(([u8; 512], SocketAddr), Instant)>,
                socket: &UdpSocket)
{
    let task = scheduled_replies.steal().success();
    let now = Instant::now();
    if task.is_some() {
        replies_and_times.push(
            (task.unwrap(),
             now.checked_add(
                 Duration::from_millis(150)).unwrap()));
    }

    // Grab the iterator to the tasks which should have been send already
    let replies_to_send_iterator = replies_and_times.iter().filter(|&task_and_time| {
        return task_and_time.1 <= now;
    });

    for reply in replies_to_send_iterator {
        let (response_buffer, data_source) = reply.0;

        match socket.send_to(&response_buffer, &data_source) {
            Err(why) => {
                println!("{:?}", why);
                continue;
            },
            Ok(_) => () // println!("Datagram sent {}", reply.1.elapsed().as_micros())
        };
    }
    // Remove sent tasks
    replies_and_times.retain(|&task_and_time| {
        return task_and_time.1 > now;
    });
}

fn logic_tick(client: &mut Client,
              delta: f64,
              speed: u16)
{
    let speed_per_tick = speed as f64 * (delta / 1000000.0);

    for (key, pressed) in &client.key_state {
        if *pressed {
            match key {
                0x57 => client.position.1 = client.position.1.sub(speed_per_tick), // W
                0x53 => client.position.1 = client.position.1.add(speed_per_tick), // S
                0x41 => client.position.0 = client.position.0.sub(speed_per_tick), // A
                0x44 => client.position.0 = client.position.0.add(speed_per_tick), // D
                _ => println!("No handling for key {}", key)
            }
        }
    }
    // restrict player position to 400,400
    client.position.0 = if client.position.0 > 400.0 { 400.0 } else { client.position.0 };
    client.position.1 = if client.position.1 > 400.0 { 400.0 } else { client.position.1 };
}

fn read_one_datagram(socket: &UdpSocket, mut buffer: [u8; 512],
                     clients: Arc<Mutex<Connections>>, last_client_id: &mut u16)
{
    let (_bytes_read, data_source) = match socket.recv_from(&mut buffer) {
        Err(why) => {
            match why.kind() {
                // This tells us that someone disconnected
                // We get this error for each datagram we sent to the guy who closed the socket
                ErrorKind::ConnectionReset => (),
                _ => println!("{:?}", why),
            }
            return;
        },
        Ok(returned_value) => (returned_value),
    };

    // If clients exists in the map, bump his timestamp
    // else register a new one
    match clients.lock().unwrap().entry(data_source) {
        Occupied(mut entry) => {
            entry.get_mut().last_datagram_timestamp = Instant::now();
        },
        Vacant(entry) => {
            entry.insert(Client::new(*last_client_id));
            *last_client_id += 1;
            println!("New player connected from {:?}", data_source);
        }
    }

    let counter = u16::from_be_bytes(*<&[u8; 2]>::try_from(&buffer[0..2])
        .expect("Failed to extract two first bytes"));

    // Only process absolutely freshest datagrams
    if clients.lock().unwrap().get(&data_source).unwrap().last_datagram_id > counter && counter != 0 {
        return;
    }

    clients.lock().unwrap().entry(data_source).and_modify(|e| { e.last_datagram_id = counter});

    let pressed_keys_count = u16::from_be_bytes(*<&[u8; 2]>::try_from(&buffer[2..4])
        .expect("Failed to extract two middle bytes"));

    // Remove all the pressed keys from the map
    clients.lock().unwrap().get_mut(&data_source).unwrap().key_state.retain(|_key, value| {
        !(*value)
    });

    // Read all the pressed keys from the datagram and change the player position
    for index in 0..pressed_keys_count {
        let i = index as usize;
        // skip 4 first bytes, those are counter and number of keys.
        let key = u16::from_be_bytes(*<&[u8; 2]>::try_from(&buffer[(4 + i*2)..(4 + i*2 + 2)])
            .expect("Failed to extract a key id"));
        clients.lock().unwrap().get_mut(&data_source).unwrap().key_state.insert(key, true);
    }
}