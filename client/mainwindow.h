#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QTime>

class QGraphicsScene;
class Player;
class QTimer;
class Datagram;
class QGraphicsRectItem;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

protected:
  void keyPressEvent(QKeyEvent *event) override;
  void keyReleaseEvent(QKeyEvent *event) override;

protected slots:
  void readPendingDatagrams();
  void update();

private:
  QPointF applyKeyToPosition(QPointF a_position, QVector<quint16> a_pressedKey, float speed = 10) const;

private:
  // TODO: Ideally this should be fetched from the server
  static inline const int SPEED = 500;
  const float PERFECT_TICK_DURATION = 1000.0/32.0;

  // TODO: There is argument to be made that I should ditch QGraphicsScene in favor of QML.
  //       Or other, more gamedev-oriented framework.
  QGraphicsScene * m_scene;
  QTimer * m_timer;                 // Main game-loop timer
  Player * m_player;
  Player * m_serverStatePlayer;
  QUdpSocket m_socket;
  // TODO: Aternatively, we could just keep a vector of pressed keys
  QMap<quint16, bool> m_keysState;  // every key can be pressed or not
  quint16 m_requestCounter = 0;     // Packet counter, send as first two bytes in every datagram

  // Saved sent datagrams for server reconciliation
  QMap<quint16, Datagram> m_unprocessedRequests;
  QMap<quint16, std::shared_ptr<Player>> m_otherPlayers;

  QTime m_lastTickUpdate;
  float m_lastSpeedPerTick;
};
#endif // MAINWINDOW_H
