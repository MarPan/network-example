#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QTimer>
#include <QNetworkDatagram>
#include <QDebug>
#include "mainwindow.h"
#include "datagram.h"
#include "player.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , m_scene{new QGraphicsScene{this}}
  , m_timer{new QTimer{this}}
  , m_player{new Player{0, 0, 50, 50}}
  , m_serverStatePlayer{new Player{0, 0, 50, 50}}
{
  auto graphicsView = new QGraphicsView{m_scene, this};
  graphicsView->setFixedSize(800, 600);
  this->setCentralWidget(graphicsView);

  graphicsView->setSceneRect(0, 0, 800, 800);
  graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  auto sceneRect = graphicsView->sceneRect();
  int step = abs(sceneRect.left() - sceneRect.right()) / 10.0;

  // vertical lines
  for (int x = sceneRect.left(); x < sceneRect.right(); x += step) {
    m_scene->addLine(x, sceneRect.bottom(), x, sceneRect.top(), QPen(QColor(0, 0, 0)));
  }

  // horizontal lines (0,0 is in top-left corner).
  step = abs(sceneRect.bottom() - sceneRect.top()) / 10.0;
  for (int y = sceneRect.top(); y < sceneRect.bottom(); y += step) {
    m_scene->addLine(sceneRect.left(), y, sceneRect.right(), y),  QPen(QColor(0, 0, 0));
  }

  for (int x = sceneRect.left(); x < sceneRect.right(); x += step) {
    for (int y = sceneRect.top(); y < sceneRect.bottom(); y += step) {
      auto textItem = m_scene->addText(QString::number(x) + ", " + QString::number(y), QFont("Times", 10, QFont::Bold));
      textItem->setPos(x, y);
      textItem->setDefaultTextColor(QColor("Red"));
      m_scene->addEllipse(x-5, y-5, 10, 10);
    }
  }

  m_scene->addItem(m_player);
  m_player->setBrush(QBrush(Qt::blue, Qt::SolidPattern));
  m_player->setPos(300, 300);
  m_scene->addItem(m_serverStatePlayer);
  m_serverStatePlayer->setBrush(QBrush(Qt::red, Qt::Dense7Pattern));
  m_serverStatePlayer->setPos(300, 300);

  connect(&m_socket, &QUdpSocket::readyRead,
          this, &MainWindow::readPendingDatagrams);

  m_timer = new QTimer(this);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
  m_timer->start(PERFECT_TICK_DURATION);
}

// Reads the incoming datagrams and sets position
void MainWindow::readPendingDatagrams()
{
    while (m_socket.hasPendingDatagrams()) {
        QNetworkDatagram datagram = m_socket.receiveDatagram();

        if (!datagram.senderAddress().isNull()) {
          auto data = datagram.data();

          static quint16 lastResponse = 0;

          QDataStream dataStream(&data, QIODevice::ReadOnly);
          dataStream.setByteOrder(QDataStream::BigEndian);

          // <last_client_datagram_id><client_id><player_count>[<player_id><posX><posY>]
          quint16 responseCounter, myId, playerCount;
          dataStream >> responseCounter >> myId >> playerCount;

          double xPos, yPos;

          for (int i = 0; i < playerCount; ++i) {
            quint16 playerId;
            double x, y;
            dataStream >> playerId >> x >> y;
            //qDebug() << "PLAYER " << playerId << " x: " << x << " y: " << y;
            if (playerId == myId) {
              xPos = x;
              yPos = y;
            } else {
              if (m_otherPlayers.contains(playerId)) {
                m_otherPlayers[playerId]->setPos(x, y);
                m_otherPlayers[playerId]->m_lastServerUpdate = QTime::currentTime();
              } else {
                qDebug() << "ADDING PLAYER " << playerId << " x: " << x << " y: " << y;
                auto newPlayer = std::make_shared<Player>(0, 0, 50, 50);
                newPlayer->setPos(x, y);
                newPlayer->setBrush(QBrush(Qt::green, Qt::SolidPattern));
                m_scene->addItem(newPlayer.get());
                m_otherPlayers.insert(playerId, newPlayer);
              }
            }
          }

          // Ignore datagrams older than our current state
          if ((responseCounter > lastResponse) || (responseCounter == 0)) {
            lastResponse = responseCounter;

            // drop unprocessedDatagrams up to the value of lastResponse
            auto unprocessedRequest = m_unprocessedRequests.begin();
            while (unprocessedRequest != m_unprocessedRequests.end()) {
              if (unprocessedRequest.key() < lastResponse) {
                unprocessedRequest = m_unprocessedRequests.erase(unprocessedRequest);
              } else {
                ++unprocessedRequest;
              }
            }
            // apply movement prediction onto position returned from server
            // so take xPos,yPos as a base. Go through each datagram and apply
            // pressed keys onto the xPos,yPos.
            QPointF position(xPos,yPos);
            auto unprocessedRequestConst = m_unprocessedRequests.constBegin();
            while (unprocessedRequestConst != m_unprocessedRequests.end()) {
              position = applyKeyToPosition(position, unprocessedRequestConst.value().pressedKeys(), SPEED * PERFECT_TICK_DURATION / 1000.0);
              ++unprocessedRequestConst;
            }

//            qDebug() << "Response " << responseCounter << " " << xPos << " " << yPos;
            m_serverStatePlayer->setPos(xPos, yPos);
            m_player->setPos(position);


            if (m_serverStatePlayer->pos() != m_player->pos()) {
              QString dbgPrint;
              for (auto it = m_unprocessedRequests.constBegin(); it != m_unprocessedRequests.constEnd(); ++it) {
                dbgPrint += "{";
                for (auto elem : it->pressedKeys()) {
                  dbgPrint += " " + QString::number(elem) + " ";
                }
                dbgPrint += "} ";
              }
              qDebug() << lastResponse << "POS MISMATCH " << m_serverStatePlayer->pos() <<  "local: " << m_player->pos() << m_unprocessedRequests.size() << dbgPrint;
            }
          }
        }
    }
}

// TODO: decouple drawing loop from logic loop
//
void MainWindow::update()
{
  auto now = QTime::currentTime();
  auto delta = m_lastTickUpdate.msecsTo(now);
  m_lastTickUpdate = now;
  // SPEED is in pixels/second. Delta needs to be converted to seconds.
  m_lastSpeedPerTick = SPEED * (delta / 1000.0);

  for (auto it = m_otherPlayers.begin(); it != m_otherPlayers.end();) {
      if (it.value()->m_lastServerUpdate.secsTo(QTime::currentTime()) >= 1) {
          it = m_otherPlayers.erase(it);
      } else {
          ++it;
      }
  }

  // 1. SEND OUT KEYBOARD INPUT
  Datagram datagram(m_requestCounter++, m_keysState);
  // TODO: after counter overflow, unprocessed requests should be cleared.
  //       either that, or the requests should be stored with the time of sending
  //       and be cleared after they're too old to be relevant.

  // Send the udp packet
  m_unprocessedRequests.insert(datagram.requestId(), datagram);
  m_socket.writeDatagram(datagram.data(), datagram.data().size(), QHostAddress("127.0.0.1"), 7878);

  // Client-side movement prediction
  auto iterator = m_keysState.constBegin();
  while (iterator != m_keysState.constEnd()) {
    // only react on pressed keys
    if (iterator.value()) {
      m_player->setPos(applyKeyToPosition(m_player->pos(), {iterator.key()}, m_lastSpeedPerTick));
    }
    ++iterator;
  }

  // 2. ANIMATE
  m_scene->advance();
}


QPointF MainWindow::applyKeyToPosition(QPointF a_position, QVector<quint16> a_pressedKeys, float speed) const
{
  auto x = a_position.x();
  auto y = a_position.y();

  for (quint16 pressedKey : a_pressedKeys) {
    switch (pressedKey) {
      case Qt::Key_W:
        y -= speed;
        break;
      case Qt::Key_S:
        y += speed;
        break;
      case Qt::Key_A:
        x -= speed;
        break;
      case Qt::Key_D:
        x += speed;
        break;
      default:
        break;
    }
  }

  return QPointF(x,y);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
  if (!event->isAutoRepeat()) {
    switch (event->key()) {
      case Qt::Key_W:
      case Qt::Key_S:
      case Qt::Key_A:
      case Qt::Key_D:
        m_keysState[event->key()] = true;
      default:
        QMainWindow::keyPressEvent(event);
        break;
    }
  }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
  if (!event->isAutoRepeat()) {
    switch (event->key()) {
      case Qt::Key_W:
      case Qt::Key_S:
      case Qt::Key_A:
      case Qt::Key_D:
        m_keysState[event->key()] = false;
        break;
      default:
        QMainWindow::keyReleaseEvent(event);
        break;
    }
  }

}

MainWindow::~MainWindow()
{
}

