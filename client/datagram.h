#ifndef DATAGRAM_H
#define DATAGRAM_H

#include <QByteArray>

class Datagram
{
public:
  Datagram(quint16 a_requestCounter, QMap<quint16, bool> a_keysState);
  Datagram(QByteArray a_datagram);

  QByteArray data() const;
  quint16 requestId() const;
  QVector<quint16> pressedKeys() const;

private:
  QByteArray m_data;
};

#endif // DATAGRAM_H
