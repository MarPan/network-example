#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
  // floating points coming from rust server are conforming with iec559 (IEEE-754)
  static_assert(std::numeric_limits<double>::is_iec559);

  QApplication a(argc, argv);
  MainWindow w;
  w.show();
  return a.exec();
}
