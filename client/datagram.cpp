#include <QMap>
#include <QDataStream>
#include "datagram.h"

Datagram::Datagram(quint16 a_requestCounter, QMap<quint16, bool> a_keysState)
{
  QDataStream dataStream(&m_data, QIODevice::WriteOnly | QIODevice::Append);
  dataStream.setByteOrder(QDataStream::BigEndian);
  dataStream << a_requestCounter;

  // Count number of pressed keys
  int pressedKeysCount = std::count_if(a_keysState.begin(), a_keysState.end(),
                                       [](const auto & value) {
      return value;
    });

  // Add it to the datagram
  dataStream << quint16(pressedKeysCount);

  // and then add keycodes of all pressed keys
  auto i = a_keysState.constBegin();
  while (i != a_keysState.constEnd()) {
    if (i.value()) {
      dataStream << i.key();
    }
    ++i;
  }
}

Datagram::Datagram(QByteArray a_datagram)
  : m_data(a_datagram)
{
}

QByteArray Datagram::data() const
{
  return m_data;
}

quint16 Datagram::requestId() const
{
  // BigEndian - first byte is a high-order one
  quint16 requestId = ((static_cast<quint16>(m_data[0]) & 0xFF) << 8 ) +
                       (static_cast<quint16>(m_data[1]) & 0xFF);
  return requestId;
}


QVector<quint16> Datagram::pressedKeys() const
{
  QVector<quint16> pressedKeys;
  // There is no reason for this being QDataStream way and requestId to be done
  // manual way apart from a teaching exercise.
  if (! m_data.isEmpty()) {
    QDataStream dataStream(m_data);
    quint16 pressedKeysCount;

    // Read two bytes twice, because we don't care about first two bytes (requestId)
    dataStream >> pressedKeysCount >> pressedKeysCount;
    for (int i = 0; i < pressedKeysCount; i++) {
      quint16 keyCode;
      dataStream >> keyCode;
      pressedKeys.append(keyCode);
    }
  }
  return pressedKeys;
}
