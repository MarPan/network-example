#ifndef PLAYER_H
#define PLAYER_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTime>

class PlayerController : public QObject
{
  Q_OBJECT

signals:


};

class Player : public QGraphicsRectItem
{
public:
  Player(double x, double y, int w, int h);


  QTime m_lastServerUpdate;

  std::unique_ptr<PlayerController> controller;

protected:

};

#endif // PLAYER_H
